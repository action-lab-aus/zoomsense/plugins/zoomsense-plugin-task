import { ZoomSensePlugin } from "@zoomsense/zoomsense-firebase";

import { Topology } from "./Topology";
import { MessageConfig } from "./MessageConfig";

export interface TaskPluginConfiguration extends ZoomSensePlugin {
  ["breakout rooms"]: Topology;
  messages?: MessageConfig[];
}