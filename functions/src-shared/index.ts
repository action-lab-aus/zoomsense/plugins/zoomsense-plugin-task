import { TaskPluginConfiguration } from "./TaskPluginConfiguration";
import { TaskPluginData } from "./TaskPluginData";

// merge this plugin's types into the core types
import "@zoomsense/zoomsense-firebase";

declare module "@zoomsense/zoomsense-firebase" {
  interface ZoomSenseConfigCurrentStatePlugins {
    task: TaskPluginConfiguration;
  }

  interface ZoomSenseDataPlugins {
    task: TaskPluginData;
  }
}

export * from "./TaskPluginConfiguration";
export * from "./TaskPluginData";

export * from "./AssignmentHistory";
export * from "./MessageConfig";
export * from "./Result";
export * from "./RoomAssignment";
export * from "./Topology";
