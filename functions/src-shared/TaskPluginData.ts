import { RoomAssignment } from "./RoomAssignment";
import { MessageConfig } from "./MessageConfig";

export interface TaskPluginData {
  [meetingId: string]: {
    "breakout rooms": {
      [breakoutRoomId: string]: {
        timestamp: number;
        section: number;
        assignment: RoomAssignment;
      };
    };
    messages: {
      [messageId: string]: {
        content: string;
        name: string;
        config: MessageConfig["config"];
        section: number;
        timestamp: number;
      };
    };
    messageEdits: {
      [messageEditEventId: string]: {
        timestamp: number;
        originalMessage: MessageConfig;
        newMessage: MessageConfig;
        messageIndex: number;
        section: number;
      };
    };
    messageCreation: {
      [messageCreationEventId: string]: {
        timestamp: number;
        message: MessageConfig;
        section: number;
      };
    };
    messageDeletion: {
      [messageDeletionId: string]: {
        timestamp: number;
        message: MessageConfig;
        section: number;
      };
    };
  };
}