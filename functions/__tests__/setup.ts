import initTest from "firebase-functions-test";
import * as admin from "firebase-admin";

// This file is automatically included by every test - see .mocharc.json `file` field.

// User emulator database

export const databaseURL =
  "http://localhost:9000/?ns=zoomsense-plugin-default-rtdb";

const projectConfig = {
  projectId: "zoomsense-plugin",
  databaseURL,
};

export const testRunner = initTest(projectConfig);

admin.initializeApp({ databaseURL });

beforeEach(() => admin.database().ref("/").remove());
