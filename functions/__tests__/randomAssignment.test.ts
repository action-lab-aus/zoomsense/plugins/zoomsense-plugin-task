import { assert } from "chai";
import { testRunner } from "./setup";
import { nextSection } from "../src/nextSection";
import { getCurrentAssignment } from "../src/db";
import {
  addCurrentUser,
  enablePlugin,
  makeSectionLoadedSnapshot,
  setBOConfig,
  TEST_SENSORS,
  TEST_USERS,
} from "./common";
import { stub } from "sinon";

const wrapped = testRunner.wrap(nextSection);

describe("randomly assigning users", () => {
  const TEST_ID = "TEST_ID";
  const changeParams = { params: { meetingId: TEST_ID } };

  it("should assign nobody, since plugin isn't enabled", async () => {
    const snap = makeSectionLoadedSnapshot(TEST_ID, 0);
    await wrapped(snap, changeParams);

    assert.deepEqual(await getCurrentAssignment(TEST_ID), {});
  });

  it("should assign nobody, since nobody is in the room", async () => {
    await enablePlugin(TEST_ID);
    const snap = makeSectionLoadedSnapshot(TEST_ID, 0);

    // Set the config for the breakout room
    await setBOConfig(TEST_ID, {
      type: "random",
      participants_per_room: 4,
    });

    await wrapped(snap, changeParams);
    assert.deepEqual(await getCurrentAssignment(TEST_ID), {});
  });

  it("should assign a single participant", async () => {
    // Enable the plugin
    await enablePlugin(TEST_ID);

    // Put one person in the meeting
    await addCurrentUser(TEST_ID, TEST_USERS[0]);

    // Set the config for the breakout room
    await setBOConfig(TEST_ID, {
      type: "random",
      participants_per_room: 4,
    });

    // The `nextSection` is triggered by a change in the currently loaded
    // section
    const snap = makeSectionLoadedSnapshot(TEST_ID, 0);
    await wrapped(snap, changeParams);

    // Assert that the user was assigned to a room by themselves
    assert.deepEqual(
      await getCurrentAssignment(TEST_ID),
      { "Room 1": [TEST_SENSORS[0], TEST_USERS[0]] }
    );
  });

  it("should create two rooms", async () => {
    await Promise.all(TEST_USERS.map((u) => addCurrentUser(TEST_ID, u)));
    // Set the config for the breakout room
    await setBOConfig(TEST_ID, {
      type: "random",
      participants_per_room: 4,
    });
    await enablePlugin(TEST_ID);

    const snap = makeSectionLoadedSnapshot(TEST_ID, 0);
    await wrapped(snap, changeParams);

    const rooms = await getCurrentAssignment(TEST_ID);

    assert.equal(Object.keys(rooms).length, 2, "Wrong number of rooms");

    Object.keys(rooms).forEach((roomName, index) => {
      assert.equal(roomName, `Room ${index + 1}`, "Bad Room name");
      // First room should have 3 users
      if (roomName === "Room 1") {
        assert.equal(
          rooms[roomName].length,
          4,
          "Wrong number of users in room"
        );
      } else if (roomName === "Room 2") {
        assert.equal(
          rooms[roomName].length,
          3,
          "Wrong number of users in room"
        );
      }
    });
  });

  it("should assign to rooms randomly", async () => {
    await enablePlugin(TEST_ID);
    await addCurrentUser(TEST_ID, TEST_USERS[0]);
    await addCurrentUser(TEST_ID, TEST_USERS[1]);
    // Set the config for the breakout room
    await setBOConfig(TEST_ID, {
      type: "random",
      participants_per_room: 1,
    });

    const snap = makeSectionLoadedSnapshot(TEST_ID, 0);

    const randomStub = stub(Math, "random").returns(0.99);

    await wrapped(snap, changeParams);

    const assignmentOne = await getCurrentAssignment(TEST_ID);

    assert.deepEqual(assignmentOne, {
      "Room 1": [TEST_SENSORS[0], TEST_USERS[0]],
      "Room 2": [TEST_SENSORS[1], TEST_USERS[1]],
    });

    randomStub.returns(0);
    await wrapped(snap, changeParams);

    const assignmentTwo = await getCurrentAssignment(TEST_ID);

    assert.deepEqual(assignmentTwo, {
      "Room 1": [TEST_SENSORS[0], TEST_USERS[1]],
      "Room 2": [TEST_SENSORS[1], TEST_USERS[0]],
    });
  });

  it("should allocate `N` rooms", async () => {
    await enablePlugin(TEST_ID);
    await setBOConfig(TEST_ID, { type: "random", room_count: 4 });
    await Promise.all(TEST_USERS.map((u) => addCurrentUser(TEST_ID, u)));
    const snap = makeSectionLoadedSnapshot(TEST_ID, 0);
    await wrapped(snap, changeParams);

    const assignment = await getCurrentAssignment(TEST_ID);

    assert.equal(Object.keys(assignment).length, 4);
    assert.equal(assignment["Room 1"].length, 3);
    assert.equal(assignment["Room 2"].length, 2);
    assert.equal(assignment["Room 3"].length, 2);
    assert.equal(assignment["Room 4"].length, 2);
  });
});
