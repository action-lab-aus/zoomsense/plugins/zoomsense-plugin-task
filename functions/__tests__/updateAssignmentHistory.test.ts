import {
  makeAssignmentChangeSnapshot,
  setCurrentSection,
  TEST_USERS,
} from "./common";
import { testRunner } from "./setup";
import { updateAssignmentHistory } from "../src/history";
import { assert } from "chai";
import { db } from "../src/db";

const wrapped = testRunner.wrap(updateAssignmentHistory);

describe("append to history on assignment change", () => {
  const TEST_ID = "TEST_ID";
  const changeParams = { params: { meetingId: TEST_ID } };
  it("should append history on assignment change", async () => {
    const assignment = { "Room One": TEST_USERS };
    await setCurrentSection(TEST_ID, 0);
    await db
      .ref(
        `config/${TEST_ID}/current/currentState/plugins/task/breakout rooms/assignment`
      )
      .set(assignment);
    const change = makeAssignmentChangeSnapshot(TEST_ID, assignment);
    await wrapped(change, changeParams);
    const history = (
      await db.ref(`data/plugins/task/${TEST_ID}/breakout rooms`).get()
    ).val()!;
    assert.exists(history);
    const keys = Object.keys(history);
    assert.equal(keys.length, 1);
    const historyId = keys[0];
    assert.equal(history[historyId].section, 0);
    assert.deepEqual(history[historyId].assignment, assignment);
    assert(history[historyId].timestamp < new Date().valueOf());
  });
});
