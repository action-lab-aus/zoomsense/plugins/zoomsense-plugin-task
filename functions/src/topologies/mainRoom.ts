import { getCurrentAttendeeUserList } from "../getCurrentAttendeeUserList";
import { RoomAssignment } from "../../src-shared";

export const generateMainRoomBreakoutRoom = async (
  meetingId: string
): Promise<RoomAssignment> => {
  const userList = await getCurrentAttendeeUserList(meetingId);

  return { "Main Room": userList };
};
