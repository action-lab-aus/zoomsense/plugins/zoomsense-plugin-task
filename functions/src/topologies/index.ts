import { generateRandomBreakoutRooms } from "./random";
import { generatePairsRoundRobinBreakoutRooms } from "./pairsRoundRobin";
import { generateMainRoomBreakoutRoom } from "./mainRoom";
import { generateRepeatBreakoutRooms } from "./repeat";
import { db } from "../db";
import { RoomAssignment, TaskPluginConfiguration, Topology } from "../../src-shared";

type AssigmentGenerator = (
  meetingId: string,
  topology: Topology
) => Promise<RoomAssignment>;

// A lookup table of assignment functions
// Signature of the functions:
//   (meetingId, sectionTopology) => Promise<breakoutRoomAssignment>
const topologyTypeToAssignmentFunction: Record<string, AssigmentGenerator> = {
  random: generateRandomBreakoutRooms,
  pairsRoundRobin: generatePairsRoundRobinBreakoutRooms,
  repeat: generateRepeatBreakoutRooms,
  "main room": generateMainRoomBreakoutRoom,
};

const sensorNamePrefix = "ZoomSensor_";

/**
 * This plugin maintains a breakout room assignment.
 * This function creates the assignment. The assignment
 * is defined in terms of the section's topology
 */
export const assignUsers = async (
  meetingId: string,
  topology: TaskPluginConfiguration["breakout rooms"]
): Promise<void> => {
  // To create the new breakout room assignments, we use an 'assigner', which
  // we look up based on the type of the session topology.
  const assigner = topologyTypeToAssignmentFunction[topology.type];

  if (!assigner) return;

  const assignment = await assigner(meetingId, topology);

  // Add ZoomSensors to the room assignments, if they're not already present.
  const { roomsWithoutSensors, usedSensorNames } = Object.keys(assignment)
    .reduce<{ roomsWithoutSensors: string[], usedSensorNames: string[] }>(({ roomsWithoutSensors, usedSensorNames }, roomName) => {
      const sensor = assignment[roomName].find((user) => (user.username.startsWith(sensorNamePrefix)));
      return {
        roomsWithoutSensors: (sensor) ? roomsWithoutSensors : [...roomsWithoutSensors, roomName],
        usedSensorNames: (sensor) ? [...usedSensorNames, sensor.username] : usedSensorNames
      }
    }, { roomsWithoutSensors: [], usedSensorNames: [] });
  const availableSensorNames = new Array(Object.keys(assignment).length)
    .fill(null)
    .map((_, index) => (
      sensorNamePrefix + (index + 1)
    ))
    .filter((sensorName) => (
      !usedSensorNames.includes(sensorName)
    ));
  for (const roomName of roomsWithoutSensors) {
    const sensorName = availableSensorNames.shift()!;
    assignment[roomName].unshift({
      username: sensorName,
      userRole: "Sensor",
      userId: ""
    });
  }

  await db
    .ref(
      `/config/${meetingId}/current/currentState/plugins/task/breakout rooms`
    )
    .update({ assignment });
};
