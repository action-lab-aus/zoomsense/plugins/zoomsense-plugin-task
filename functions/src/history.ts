import * as functions from "firebase-functions";
import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase-server";

import { db } from "./db";
import { MessageConfig } from "../src-shared";

export const updateAssignmentHistory = typedFirebaseFunctionRef(
  functions.database.ref
)(
  "config/{meetingId}/current/currentState/plugins/task/breakout rooms/assignment"
).onWrite(async (change, context) => {
  if (!change.after.exists()) {
    // Do nothing when the assignment config is deleted.
    return;
  }

  const { meetingId } = context.params;

  const sectionNum = (
    await db.ref(`config/${meetingId}/current/currentSection`).get()
  ).val();

  if (sectionNum === null) {
    return;
  }

  const now = new Date().getTime();
  await db.ref(`data/plugins/task/${meetingId}/breakout rooms`).push({
    timestamp: now,
    section: sectionNum,
    assignment: change.after.val(),
  });
});

export const updateMessageHistory = typedFirebaseFunctionRef(functions.database.ref)(
  "/config/{meetingId}/current/currentState/plugins/task/messages/{messageIndex}/sent"
).onWrite(async (change, context) => {
  const timestamp = change.after.val();
  if (!timestamp) {
    return;
  }

  const { meetingId, messageIndex } = context.params;

  const message = (
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
      )
      .get()
  ).val();

  if (message == null) {
    console.error(
      "Couldn't find message with message index",
      messageIndex,
      "for meeting",
      meetingId
    );
    return;
  }

  const section = (
    await db.ref(`config/${meetingId}/current/currentSection`).get()
  ).val();

  if (section === null) {
    return;
  }

  await db.ref(`data/plugins/task/${meetingId}/messages`).push({
    timestamp,
    content: message.content,
    config: message.config,
    name: message.name,
    section,
  });
});

export const pushEditMessageHistoryEntry = async (
  originalMessage: MessageConfig,
  newMessage: MessageConfig,
  meetingId: string,
  messageIndex: number
): Promise<void> => {
  const section = (
    await db.ref(`config/${meetingId}/current/currentSection`).get()
  ).val();

  if (section === null) {
    return;
  }

  await db.ref(`/data/plugins/task/${meetingId}/messageEdits`).push({
    timestamp: new Date().valueOf(),
    originalMessage,
    newMessage,
    messageIndex,
    section,
  });
};

export const pushNewMessageHistoryEvent = async (
  meetingId: string,
  message: MessageConfig
): Promise<void> => {
  const section = (
    await db.ref(`config/${meetingId}/current/currentSection`).get()
  ).val();

  if (section === null) {
    return;
  }
  await db.ref(`data/plugins/task/${meetingId}/messageCreation`).push({
    timestamp: new Date().valueOf(),
    message,
    section,
  });
};

export const pushDeleteMessageHistoryEvent = async (
  meetingId: string,
  message: MessageConfig
): Promise<void> => {
  const section = (
    await db.ref(`config/${meetingId}/current/currentSection`).get()
  ).val();

  if (section === null) {
    return;
  }
  await db.ref(`/data/plugins/task/${meetingId}/messageDeletion`).push({
    timestamp: new Date().valueOf(),
    message,
    section,
  });
};
