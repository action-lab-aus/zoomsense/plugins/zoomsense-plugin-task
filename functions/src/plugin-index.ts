export * as Task from "./index";

// Also export the core ZoomSense functions, so the emulator has them all.
// @ts-ignore
export * from "../../../../zoomsense-functions/functions/lib";