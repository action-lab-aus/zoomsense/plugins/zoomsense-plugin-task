import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase-server";
import * as functions from "firebase-functions";

import { assignParticipantIfLateJoiner } from "./assignParticipantIfLateJoiner";
import { db } from "./db";

export const assignLateJoiners = typedFirebaseFunctionRef(functions.database.ref)(
  "data/logs/entryExit/{meetingId}/{sensorId}/{entryExitId}"
).onCreate(async (value, context) => {
  const entryExit = value.val();
  const { meetingId } = context.params;

  const enabled = await db.ref(
    `config/${meetingId}/current/currentState/plugins/task/enabled`
  ).get();
  // If the plugin isn't enabled or the entry exit log
  // is malformed, don't do anything
  if (!entryExit || !enabled.val()) {
    return;
  }

  await assignParticipantIfLateJoiner(entryExit, meetingId);
});
