import * as admin from "firebase-admin";
admin.initializeApp();

export { nextSection } from "./nextSection";

export { assignLateJoiners } from "./assignLateJoiners";

export { handleRename } from "./handleRename";

export { updateMessageHistory, updateAssignmentHistory } from "./history";

export {
  editMessage,
  sendMessage,
  deleteMessage,
  onCurrentSectionStarted,
  onSectionEndAtTimestampCreated,
  onSectionEndAtTimestampDeleted,
  onMessageSendFlagSet,
} from "./messages";

export { newMessage } from "./newMessage";

export { renameAssignment } from "./renameAssignment";
