import * as functions from "firebase-functions";
import { typedFirebaseFunctionRef } from "@zoomsense/zoomsense-firebase-server";
import humanInterval from "human-interval";

import {
  db,
  setMessage,
  getZoomSensorsInBreakoutRooms,
  getCurrentSectionEndTime,
  cancelScheduledSendMessage,
  scheduleSendMessage,
} from "./db";
import {
  pushDeleteMessageHistoryEvent,
  pushEditMessageHistoryEntry,
} from "./history";
import { err, ErrorOr, isMessageConfig, MessageConfig, succ } from "../src-shared";

const HttpsError = functions.https.HttpsError;

/**
 * Actually send a message when the `send` path in the message is created.
 */
export const onMessageSendFlagSet = typedFirebaseFunctionRef(functions.database.ref)(
  "config/{meetingId}/current/currentState/plugins/task/messages/{messageIndex}/send"
).onCreate(async (_snapshot, context) => {
  const { meetingId, messageIndex } = context.params;

  const message = (
    await db
      .ref(
        `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
      )
      .get()
  ).val();

  if (!message) {
    return;
  }

  const chats = (await db.ref(`/data/chats/${meetingId}`).get()).val();

  if (chats === null) {
    return;
  }

  const zoomSensorsInBreakoutRooms = await getZoomSensorsInBreakoutRooms(meetingId);

  await sendMessageImpl(meetingId, message, messageIndex, zoomSensorsInBreakoutRooms);
});

/**
 * Test if the message has a known delivery time given the information provided, and if so, schedule it to be delivered.
 */
async function scheduleMessageToSend(message: MessageConfig, messageIndex: number, meetingId: string,
                                     currentSectionStartTime: number | null, currentSectionEndTime: number | null): Promise<void> {

  const { config: { time, from }, sent } = message;
  if (!time || !from || sent) return;

  const timeMilliseconds = humanInterval(time);
  if (timeMilliseconds === undefined) {
    return;
  }

  let timestamp: number;

  if (from === "start" && currentSectionStartTime !== null) {
    timestamp = currentSectionStartTime + timeMilliseconds;
  } else if (from === "end") {
    if (currentSectionEndTime !== null) {
      timestamp = currentSectionEndTime - timeMilliseconds;
    } else {
      if (message.sendJobId) {
        // A job to send the message was scheduled, but the end time is no longer set (i.e. the timer has been paused).
        await cancelScheduledSendMessage(meetingId, messageIndex);
      }
      return;
    }
  } else {
    return;
  }

  await scheduleSendMessage(timestamp, meetingId, messageIndex);
}

// Schedule updates to send all pending messages for the given meeting
async function schedulePendingMessagesToSend(meetingId: string, currentSectionStartTime: number | null, currentSectionEndTime: number | null): Promise<void> {
  const enabled = (
    await db.ref(`config/${meetingId}/current/currentState/plugins/task/enabled`).get()
  ).val();

  if (!enabled) {
    return;
  }

  const messages = (
    await db
      .ref(`config/${meetingId}/current/currentState/plugins/task/messages`)
      .get()
  ).val();

  if (messages === null) {
    return;
  }

  // Shortcircuit if there are no messages for this meeting section
  if (!messages.length) return;

  await Promise.all(messages
    .map((message, messageIndex) => (
      scheduleMessageToSend(message, messageIndex, meetingId, currentSectionStartTime, currentSectionEndTime)
    )));
}

export const onCurrentSectionStarted = typedFirebaseFunctionRef(functions.database.ref)(
  "config/{meetingId}/current/currentState/startedAt"
).onUpdate(async (change, context) => {
  const meetingId = context.params.meetingId;

  const enabled = (
    await db
      .ref(`config/${meetingId}/current/currentState/plugins/task/enabled`)
      .get()
  ).val();

  if (!enabled) {
    return;
  }

  const currentSectionStartTime = change.after.val();

  const currentSectionEndTimeOrError = await getCurrentSectionEndTime(
    meetingId
  );
  if (currentSectionEndTimeOrError.isErr) {
    console.error(currentSectionEndTimeOrError.err);
    return;
  }
  const currentSectionEndTime = currentSectionEndTimeOrError.data;

  await schedulePendingMessagesToSend(meetingId, currentSectionStartTime, currentSectionEndTime);
});

export const onSectionEndAtTimestampCreated = typedFirebaseFunctionRef(functions.database.ref)(
  "config/{meetingId}/current/currentState/endsAt"
).onCreate(async (snapshot, context) => {
  const currentSectionEndTime = Number(snapshot.val());
  const meetingId = context.params.meetingId;

  await schedulePendingMessagesToSend(meetingId, null, currentSectionEndTime);
});

export const onSectionEndAtTimestampDeleted = typedFirebaseFunctionRef(functions.database.ref)(
  "config/{meetingId}/current/currentState/endsAt"
).onDelete(async (_snapshot, context) => {
  const meetingId = context.params.meetingId;

  // Any messages which are sent relative to the end of the section need to be cancelled.
  await schedulePendingMessagesToSend(meetingId, null, null);
});

export const editMessage = functions.https.onCall(async (data, context) => {
  if (!context.auth?.token) {
    return new HttpsError("unauthenticated", "unauthenticated");
  }

  const {
    // When we edit the message, we will remove the `send` property.
    // The one in the request may or may not have the `send` property,
    // hence the name
    editedMessage: editedMessageWithSend,
    messageIndex,
    meetingId,
  } = data;

  const messageExists = (
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
      )
      .get()
  ).exists();

  if (!messageExists) {
    return new HttpsError(
      "invalid-argument",
      `Message with index ${messageIndex} doesn't exist for meeting ${meetingId}`
    );
  }

  if (!isMessageConfig(editedMessageWithSend)) {
    return new HttpsError(
      "invalid-argument",
      "Malformed message config. `editMessage` didn't have the expected properties"
    );
  }

  const originalMessage = (
    await db.ref(`/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`).get()
  ).val();

  if (!originalMessage) {
    throw new HttpsError(
      "failed-precondition",
      "The message you are editing has been deleted"
    );
  }

  // Remove the `send` property and set `sent` to null
  const { send, ...editedMessageWithoutSend } = editedMessageWithSend;
  const editedMessage = {
    ...editedMessageWithoutSend,
    sent: null,
  };

  await setMessage(meetingId, messageIndex, editedMessage);

  await pushEditMessageHistoryEntry(
    originalMessage,
    editedMessage,
    meetingId,
    messageIndex
  );
  return "OK";
});

// Handler for user requesting to send manual message
export const sendMessage = functions.https.onCall(async (data, context) => {
  if (!context.auth?.token) {
    return new HttpsError("unauthenticated", "unauthenticated");
  }

  const { messageIndex, meetingId } = data;

  const message = (
    await db.ref(`/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`).get()
  ).val();

  if (!message) {
    throw new HttpsError(
      "failed-precondition",
      "The message you are viewing has been deleted."
    );
  }

  if (message.config.from !== "manual") {
    console.error(
      `[${meetingId}] - tried to manually send non-manual message: "${message}"`
    );
    throw new HttpsError(
      "failed-precondition",
      `Can't manually send a non-manual message: ${JSON.stringify(message)}`
    );
  }

  const rooms = await getZoomSensorsInBreakoutRooms(meetingId);

  const sendMessageResult = await sendMessageImpl(
    meetingId,
    message,
    messageIndex,
    rooms
  );
  if (sendMessageResult.isErr) {
    console.error(sendMessageResult.err);
    return new HttpsError("internal", "Server Error");
  }

  return "OK";
});

const broadcastMessageToRooms = async (
  meetingId: string,
  message: MessageConfig,
  roomsToBroadcastTo: string[]
): Promise<void> => {
  const { content } = message;
  // Wait for all messages to be sent
  await Promise.all(
    roomsToBroadcastTo.map(async (zoomSensor) =>
      // Send the message in all breakout rooms
      db.ref(`data/chats/${meetingId}/${zoomSensor}/message`).push({
        msg: content,
        receiver: 0,
      })
    )
  );
};

const sendMessageImpl = async (
  meetingId: string,
  message: MessageConfig,
  messageIndex: number,
  zoomSensorsInBreakoutRooms: string[]
): Promise<ErrorOr<undefined>> => {
  if (message.config.type === "broadcast") {
    await broadcastMessageToRooms(
      meetingId,
      message,
      zoomSensorsInBreakoutRooms
    );
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/sent`
      )
      .set(new Date().valueOf());
    await db
      .ref(
        `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/send`
      )
      .remove();

    return succ(undefined);
  }

  if (message.config.type === "room") {
    if (!message.config.sensors) {
      return err(
        `[${meetingId}] - Couldn't send message ${JSON.stringify(
          message
        )}, because the type is "${
          message.config.type
        }", but a sensor wasn't specified (message.config.sensors == ${
          message.config.sensors
        })`
      );
    }
    const receiverSensors = message.config.sensors.split(/,\s*/);
    const boRoomSensors = receiverSensors.filter((sensor) =>
      zoomSensorsInBreakoutRooms.includes(sensor)
    );
    if (boRoomSensors.length === 0) {
      let messageText = `[${meetingId}] - Couldn't send message ${JSON.stringify(
        message
      )}`;
      if (receiverSensors.length === 1) {
        messageText += `, because "${receiverSensors[0]}" is not in a breakout room.`;
      } else {
        messageText += `, because none of "${message.config.sensors}" are in breakout rooms.`;
      }
      return err(messageText);
    }

    await broadcastMessageToRooms(meetingId, message, boRoomSensors);
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/sent`
      )
      .set(new Date().valueOf());
    await db
      .ref(
        `config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}/send`
      )
      .remove();

    return succ(undefined);
  }

  return err(`Unsupported config type: ${message.config.type}`);
};

export const deleteMessage = functions.https.onCall(async (data, context) => {
  if (!context.auth?.token) {
    return new HttpsError("unauthenticated", "unauthenticated");
  }
  const { meetingId, messageIndex } = data;
  const messageExists = (
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
      )
      .get()
  ).exists();

  if (!messageExists) {
    return new HttpsError(
      "invalid-argument",
      `Message with index ${messageIndex} doesn't exist for meeting ${meetingId}`
    );
  }

  const message = (
    await db
      .ref(
        `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
      )
      .get()
  ).val();

  if (message === null) {
    return new HttpsError("internal", "internal");
  }

  await db
    .ref(
      `/config/${meetingId}/current/currentState/plugins/task/messages/${messageIndex}`
    )
    .remove();

  await pushDeleteMessageHistoryEvent(meetingId, message);

  return "OK";
});
