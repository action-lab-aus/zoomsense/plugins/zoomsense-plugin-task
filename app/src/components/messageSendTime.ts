import humanInterval from "human-interval";
import { MessageConfig } from "zoomsense-plugin-task-shared";

// Get the send time of the given message
// Returns `undefined` if there is no send time,
// otherwise returns a number that is the send time
// time stamp (this may be in the past if the message
// is already sent, or in the future)
/**
 * @param {MessageConfig} message
 * @param {number} currentSectionStartTime
 * @param {number | null} currentSectionEndTime
 * @returns {number | undefined}
 */
export const messageSendTime = (
  message: MessageConfig,
  currentSectionStartTime: number,
  currentSectionEndTime: number | null
) => {
  // Manual messages don't have a deteminate send time
  if (message.config.from === "manual") return undefined;
  // If the current section doesn't have an end time, and the message send time
  // is relative to the end of the section, than the message will never be sent
  if (message.config.from === "end" && Number.isNaN(currentSectionEndTime))
    return undefined;

  // In the config, the time is a string e.g. "1 minute"
  const messageTimeStamp = humanInterval(message.config.time);

  if (!messageTimeStamp || Number.isNaN(messageTimeStamp)) return;

  if (message.config.from === "end" && currentSectionEndTime !== null) {
    return currentSectionEndTime - messageTimeStamp;
  }
  if (message.config.from === "start") {
    return currentSectionStartTime + messageTimeStamp;
  }

  // We don't support other `message.config.from` values at the moment
  return undefined;
};
