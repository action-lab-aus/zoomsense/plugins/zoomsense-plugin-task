# Current State of Plugins from the perspective of Week 10 Activities

## Things to consider for Task Plugin:

- [X] Renaming the key for pairs rotation : "pairs_rotation" -> "Pairs Round Robin" (which will involve looking
  for that string hard coded in a few places he he)
- [ ] Consider allowing sending timed messages instantly
- [ ] Consider the situation where a message is broadcast before a participant has joined their room
- [ ] Will there always be a sensor in the main room?
- [ ] When someone joins late and people are already paired off, consider how to assign them - let's send an alert
  to the tutor to let them know that someone needs assignment
- [X] Have an option for the timer for transitions/messages etc. be controlled by a button the dashboard, rather
  than using `type: timed`, let's use `type: auto`, and control the transition internally from the Task plugin

## Things to consider for Voting Plugin:

- [ ] Import all existing chat submissions (that fit criteria) at start of chat submission script item (currently, we
  only pick up new ones)

